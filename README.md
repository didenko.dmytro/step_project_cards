# Step_Project_Cards

## Дмитро Діденко
Класи Button, Modal, Card, CardCardiologist, CardDentist, CardTherapist, Edit, EditCargiologist,EditTherapist. Функції appStart, removeElement, createButton, buttonModalCloseHandler, buttonEditCloseHandler,buttonModalCreateVisitHandler,buttonModalSendVisitHandler,createElement,escapeHTML(modified),filterCards,removeAllElements, removeArrElem, showCards, visitValidator.Функції по роботі з API (cardAdd, deleteCard, editCard, getCards).

## Ярослав Приступлюк
Оформлення (всі scss файли). Сітка - гріди, сайт розтягується по ширині в залежності від кількості карток) Клас ModalEnter (відповідає за роширення модалки і створення вікна авторизації). Функції 
buttonEnterHandler, buttonModalEnterHandler. Функції по роботі з API (authorization).

## Дмитро Музичук
Класи ModalCreateVisit, Visit, VisitDentist, VisitCardiologist, VisitTherapist (відповідають за розширення модалки для створення візитів);


